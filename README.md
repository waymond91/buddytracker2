Buddy Tracker To-Do's   
------------
### Schematic Checks
- [x] ICS43434 (MEMs mic) circuit
- [ ] ICS4343 connection to ESP32
- [ ] MAX98357AETE circuit
- [ ] MAX98357AETE connection to ESP32
- [ ] Buzzer pin mapping
- [ ] ADXL345 interrupt pins
- [ ] GPS Board Interconnect
- [ ] USB Micro footprint overhangs board
