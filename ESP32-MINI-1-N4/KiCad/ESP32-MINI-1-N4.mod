PCBNEW-LibModule-V1  2021-10-05 21:04:57
# encoding utf-8
Units mm
$INDEX
ESP32MINI1N4
$EndINDEX
$MODULE ESP32MINI1N4
Po 0 0 0 15 615cafe9 00000000 ~~
Li ESP32MINI1N4
Cd ESP32-MINI-1-N4-5
Kw Integrated Circuit
Sc 0
At SMD
AR 
Op 0 0 0
T0 5.900 2.1 1.27 1.27 0 0.254 N V 21 N "IC**"
T1 5.900 2.1 1.27 1.27 0 0.254 N I 21 N "ESP32MINI1N4"
DS -0.7 11.6 12.5 11.6 0.1 24
DS 12.5 11.6 12.5 -7.4 0.1 24
DS 12.5 -7.4 -0.7 -7.4 0.1 24
DS -0.7 -7.4 -0.7 11.6 0.1 24
DS -0.7 -7.4 12.5 -7.4 0.2 21
DS 12.5 -7.4 12.5 -7.4 0.2 21
DS 12.5 -7.4 -0.7 -7.4 0.2 21
DS -0.7 -7.4 -0.7 -7.4 0.2 21
DS -0.7 11.6 0.15 11.6 0.2 21
DS 0.15 11.6 0.15 11.6 0.2 21
DS 0.15 11.6 -0.7 11.6 0.2 21
DS -0.7 11.6 -0.7 11.6 0.2 21
DS 11.4 11.6 12.5 11.6 0.2 21
DS 12.5 11.6 12.5 11.6 0.2 21
DS 12.5 11.6 11.4 11.6 0.2 21
DS 11.4 11.6 11.4 11.6 0.2 21
DS -1.7 -8.4 13.5 -8.4 0.1 24
DS 13.5 -8.4 13.5 12.6 0.1 24
DS 13.5 12.6 -1.7 12.6 0.1 24
DS -1.7 12.6 -1.7 -8.4 0.1 24
DS -0.7 -7.4 -0.7 -7.4 0.2 21
DS -0.7 -7.4 -0.7 -0.65 0.2 21
DS -0.7 -0.65 -0.7 -0.65 0.2 21
DS -0.7 -0.65 -0.7 -7.4 0.2 21
DS 12.5 -7.4 12.5 -7.4 0.2 21
DS 12.5 -7.4 12.5 -0.65 0.2 21
DS 12.5 -0.65 12.5 -0.65 0.2 21
DS 12.5 -0.65 12.5 -7.4 0.2 21
DS -0.7 10.1 -0.7 10.1 0.2 21
DS -0.7 10.1 -0.7 11.6 0.2 21
DS -0.7 11.6 -0.7 11.6 0.2 21
DS -0.7 11.6 -0.7 10.1 0.2 21
DS 12.5 10.1 12.5 10.1 0.2 21
DS 12.5 10.1 12.5 11.6 0.2 21
DS 12.5 11.6 12.5 11.6 0.2 21
DS 12.5 11.6 12.5 10.1 0.2 21
DS -1.4 -0 -1.4 -0 0.2 21
DS -1.3 -0 -1.3 -0 0.2 21
DS -1.4 -0 -1.4 -0 0.2 21
DA -1.35 -0 -1.400 -0 -1800 0.2 21
DA -1.35 -0 -1.300 -0 -1800 0.2 21
DA -1.35 -0 -1.400 -0 -1800 0.2 21
$PAD
Po 0.000 -0
Sh "1" R 0.400 0.800 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 0.000 0.8
Sh "2" R 0.400 0.800 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 0.000 1.6
Sh "3" R 0.400 0.800 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 0.000 2.4
Sh "4" R 0.400 0.800 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 0.000 3.2
Sh "5" R 0.400 0.800 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 0.000 4
Sh "6" R 0.400 0.800 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 0.000 4.8
Sh "7" R 0.400 0.800 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 0.000 5.6
Sh "8" R 0.400 0.800 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 0.000 6.4
Sh "9" R 0.400 0.800 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 0.000 7.2
Sh "10" R 0.400 0.800 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 0.000 8
Sh "11" R 0.400 0.800 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 0.000 8.8
Sh "12" R 0.400 0.800 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 0.000 9.6
Sh "13" R 0.400 0.800 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 0.700 10.85
Sh "14" R 0.400 0.800 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 1.500 10.85
Sh "15" R 0.400 0.800 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 2.300 10.85
Sh "16" R 0.400 0.800 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 3.100 10.85
Sh "17" R 0.400 0.800 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 3.900 10.85
Sh "18" R 0.400 0.800 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 4.700 10.85
Sh "19" R 0.400 0.800 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 5.500 10.85
Sh "20" R 0.400 0.800 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 6.300 10.85
Sh "21" R 0.400 0.800 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 7.100 10.85
Sh "22" R 0.400 0.800 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 7.900 10.85
Sh "23" R 0.400 0.800 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 8.700 10.85
Sh "24" R 0.400 0.800 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 9.500 10.85
Sh "25" R 0.400 0.800 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 10.300 10.85
Sh "26" R 0.400 0.800 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 11.100 10.85
Sh "27" R 0.400 0.800 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 11.800 9.6
Sh "28" R 0.400 0.800 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 11.800 8.8
Sh "29" R 0.400 0.800 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 11.800 8
Sh "30" R 0.400 0.800 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 11.800 7.2
Sh "31" R 0.400 0.800 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 11.800 6.4
Sh "32" R 0.400 0.800 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 11.800 5.6
Sh "33" R 0.400 0.800 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 11.800 4.8
Sh "34" R 0.400 0.800 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 11.800 4
Sh "35" R 0.400 0.800 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 11.800 3.2
Sh "36" R 0.400 0.800 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 11.800 2.4
Sh "37" R 0.400 0.800 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 11.800 1.6
Sh "38" R 0.400 0.800 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 11.800 0.8
Sh "39" R 0.400 0.800 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 11.800 -0
Sh "40" R 0.400 0.800 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 11.100 -1.25
Sh "41" R 0.400 0.800 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 10.300 -1.25
Sh "42" R 0.400 0.800 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 9.500 -1.25
Sh "43" R 0.400 0.800 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 8.700 -1.25
Sh "44" R 0.400 0.800 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 7.900 -1.25
Sh "45" R 0.400 0.800 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 7.100 -1.25
Sh "46" R 0.400 0.800 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 6.300 -1.25
Sh "47" R 0.400 0.800 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 5.500 -1.25
Sh "48" R 0.400 0.800 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 4.700 -1.25
Sh "49" R 0.400 0.800 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 3.900 -1.25
Sh "50" R 0.400 0.800 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 3.100 -1.25
Sh "51" R 0.400 0.800 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 2.300 -1.25
Sh "52" R 0.400 0.800 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 1.500 -1.25
Sh "53" R 0.400 0.800 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 0.700 -1.25
Sh "54" R 0.400 0.800 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 5.900 4.8
Sh "55" R 4.000 4.000 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$EndMODULE ESP32MINI1N4
$EndLIBRARY
